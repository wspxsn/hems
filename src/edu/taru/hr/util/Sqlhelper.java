package edu.taru.hr.util;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
public  class Sqlhelper {
	private static String  DRIVER="driver";
	private static String  URL="url";
	private static String  USER="user";
	private static String  PASSWORD="password";
	
	private static Properties properties=new Properties();
	/*public static final String URL="jdbc:mysql://localhost/test?useUnicode=true&characterEncoding=utf8&useSSL=false";
	public static final String USER="root";
	public static final String PASSWORD="admin";*/
	//public static final ResultSet rs = null;
	//public static final Connection conn=null;//全局唯一
	static ThreadLocal<Connection> local=new ThreadLocal<Connection>() ;//本地变量
	//读
	static {
		try {
			properties.load(Sqlhelper.class.getClassLoader().getResourceAsStream("jdbc.properties"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
//加载驱动
  static {
	
	try {
		Class.forName(properties.getProperty(DRIVER));
	} catch (ClassNotFoundException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
}
	//打开连接
	public static Connection openConnection(){
		Connection conn=local.get();
		try {
			if(conn==null || conn.isClosed()){
				conn=DriverManager.getConnection(properties.getProperty(URL),properties.getProperty(USER),properties.getProperty(PASSWORD));
				
				local.set(conn);
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return conn;
	}
	//查询
	//ResultSet是结果集游标，不能直接关闭
	public static List<HashMap<String,Object>> select1(String sql, Object...params){

		Connection conn=Sqlhelper.openConnection();
		PreparedStatement pst=null;
		 ResultSet rs=null;
		 //一个记录是一个HashMap
		 List <HashMap<String,Object>> row=new ArrayList<HashMap<String,Object>>();
		 try {
			 pst=  conn.prepareStatement(sql);
			if(params!=null){
				for(int i=0;i<params.length;i++){
					pst.setObject(i+1, params[i]);
				}
			}
			    rs= pst.executeQuery();
			   ResultSetMetaData rsmete= rs.getMetaData();//获取列头元素
			  int length= rsmete.getColumnCount();//获取列的数量
			  //外层循环行，内层循环列
			   while(rs.next()){
				   HashMap<String,Object> map=new HashMap<String,Object>();
				   for(int i=0;i<length;i++){
					  String columnLabel= rsmete.getColumnLabel(i+1);//取出列的标签名
					  map.put(columnLabel, rs.getObject(columnLabel));//根据列的标签获取RS中的数据
				   }
				   row.add(map);
			   }
			    
			   // row= handlerMapper.maping(rs);//数据解析完毕  
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			if(rs!=null){
				try {
					rs.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					throw new RuntimeException("查询失败",e);
				}
				if(pst!=null){
					try {
						pst.close();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}
		
		return row;
	
	}
	
	public static  <T> List<T> select(String sql, RowHandlerMapper<T>  handlerMapper,Object...params){
		Connection conn=Sqlhelper.openConnection();
		PreparedStatement pst=null;
		 ResultSet rs=null;
		 List <T> row=null;
		 try {
			 pst=  conn.prepareStatement(sql);
			if(params!=null){
				for(int i=0;i<params.length;i++){
					pst.setObject(i+1, params[i]);
				}
			}
			    rs= pst.executeQuery();
			    row= handlerMapper.maping(rs);//数据解析完毕  
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			if(rs!=null){
				try {
					rs.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				if(pst!=null){
					try {
						pst.close();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						throw new RuntimeException("查询失败",e);
					}
				}
			}
		}
		
		return row;
	} 
	//更新
	public static int  update (String sql,Object...params){
		PreparedStatement pst=null;
		Connection conn=Sqlhelper.openConnection();
		 int rs=0;
		 try {
			 pst=  conn.prepareStatement(sql);
			if(params!=null){
				for(int i=0;i<params.length;i++){
					pst.setObject(i+1, params[i]);
				}
			}
			
			    rs= pst.executeUpdate();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			if(pst!=null){
				try {
					pst.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					throw new RuntimeException("更新失败",e);
				}
			}
		}
		return rs;
	}
	
	public static void close(){
		Connection conn=local.get();
		if(conn!=null){
			try {
				conn.close();
				local.remove();
				conn=null;
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	//接口辅助解析ResultSet
	 public  interface RowHandlerMapper<T> {
	 	 public   List<T>  maping(ResultSet rs);
	 }
}
