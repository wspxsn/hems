package edu.taru.hr.dao;

import java.util.HashMap;
import java.util.List;

import edu.taru.hr.entity.User;
import edu.taru.hr.util.Sqlhelper;

public class LoginDaoImpl {
	public User selectUser(String username,String password){
		 User user=null;
		String sql="select * from User where username=? and password=?";
		
		List<HashMap<String ,Object>>  list=Sqlhelper.select1(sql, username,password);
	
		 if(list!=null&&list.size()>0){
			 HashMap<String ,Object>  map= list.get(0);
			  user=new User();
			 user.setId(map.get("id")==null?null:(map.get("id").toString()));
			 user.setName(map.get("name")==null?null:(map.get("name").toString()));
			 user.setUsername(map.get("username")==null?null:(map.get("username").toString()));
			 user.setPassword(map.get("password")==null?null:(map.get("password").toString()));
			 user.setEmail(map.get("email")==null?null:(map.get("email").toString()));
			 user.setTelphone(map.get("telphone")==null?null:(map.get("telphone").toString()));
		 }
	
		
		
		return user;
	}
}
