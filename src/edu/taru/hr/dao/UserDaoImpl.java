package edu.taru.hr.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter.DEFAULT;

import edu.taru.hr.entity.User;
import edu.taru.hr.util.Sqlhelper;

public class UserDaoImpl {
    public void inster (User user){
    	String sql="insert into user values(default,?,?,?,?,?,?,?)";
    	
    	Sqlhelper.update(sql,user.getName(),user.getUsername(),user.getPassword(),user.getEmail(),user.getTelphone(),user.getSex(),user.getCity());
   }
    /**
     * 查询的过程（HashMap通用数据，但是界面用的user,所以必须把HashMap的数据转换成User）
     * @return
     */
     //修改搜索
    public User alterReserch(String id){
    	String sql="select * from User where id=?";
    	List<HashMap<String,Object>> list=Sqlhelper.select1(sql,id);
    	User user=null;
    	if(list.size()>0&&list!=null){
    		HashMap<String,Object> map=list.get(0);
    			 user=new User() ;
    			 user.setId(map.get("id")==null?null:map.get("id").toString());
    			 user.setName(map.get("name")==null?null:(map.get("name").toString()));
    			 user.setUsername(map.get("username")==null?null:(map.get("username").toString()));
    			 user.setPassword(map.get("password")==null?null:(map.get("password").toString()));
    			 user.setEmail(map.get("email")==null?null:(map.get("email").toString()));
    			 user.setTelphone(map.get("telphone")==null?null:(map.get("telphone").toString()));
    			 user.setSex(map.get("sex")==null?null:(map.get("sex").toString()));
    			 user.setCity(map.get("city")==null?null:(map.get("city").toString()));
    		
    	}
    	return user;
    }
    
    /**
     * 刷新
     * vector
     * @return
     */
    public Vector<Vector<String>> select1(){
    	String sql="select  * from User";
    	List<HashMap<String,Object>> list=Sqlhelper.select1(sql);
    	Vector<Vector<String>>  users=new Vector<Vector<String>>();
    	if(list.size()>0){
    		for(HashMap<String,Object> map:list){
    			
    			Vector<String> vector=new Vector<String>();
    			 vector.add(map.get("id")==null?null:map.get("id").toString());
    			 vector.add(map.get("name")==null?null:(map.get("name").toString()));
    			 vector.add(map.get("username")==null?null:(map.get("username").toString()));
    			 
    			 vector.add(map.get("email")==null?null:(map.get("email").toString()));
    			 vector.add(map.get("telphone")==null?null:(map.get("telphone").toString()));
    			 vector.add(map.get("sex")==null?null:(map.get("sex").toString()));
    			 vector.add(map.get("city")==null?null:(map.get("city").toString()));
    			 users.add(vector);
    		}
    	}
    	return users;
    }
    
    
  //搜索
    public Vector<Vector<String>> reserch(String s){
    	String sql="select * from user where id=? or name like ? escape'*'";
    	List<HashMap<String,Object>> list=Sqlhelper.select1(sql,s,"%*"+s+"%");
    	Vector<Vector<String>>  users=new Vector<Vector<String>>();
    	if(list.size()>0){
    		for(HashMap<String,Object> map:list){
    			
    			Vector<String> vector=new Vector<String>();
    			vector.add(map.get("id")==null?null:map.get("id").toString());
    			vector.add(map.get("name")==null?null:(map.get("name").toString()));
    			vector.add(map.get("username")==null?null:(map.get("username").toString()));
    			
    			vector.add(map.get("email")==null?null:(map.get("email").toString()));
    			vector.add(map.get("telphone")==null?null:(map.get("telphone").toString()));
    			vector.add(map.get("sex")==null?null:(map.get("sex").toString()));
    			vector.add(map.get("city")==null?null:(map.get("city").toString()));
    			 users.add(vector);
    		}
    	}
    	return users;
    }
	
    //删除
    public  void delect(String id){
    	String sql="delete  from user where id=?";
    	Sqlhelper.update(sql, id);
    }
  //更新
    public void update(User user){
    	String sql ="update user set name=?,username=?,email=?,telphone=? where id=?";
    	Sqlhelper.update(sql, user.getName(),user.getUsername(),user.getEmail(),user.getTelphone(),user.getId());
    }
    
    /**
     * 多条件查询
     */
    
    public Vector<Vector<String>> selectByOther(String name,String email,String telphone){
    	StringBuffer sql=new StringBuffer("select * from user where 1=1 ");
    	if(name!=null&&!name.equals("")){
    		sql.append(" and name='"+name+"'");
    	}
    	if(email!=null&&!email.equals("")){
    		sql.append(" and email='"+email+"'");
    	}
    	if(telphone!=null&&!telphone.equals("")){
    		sql.append(" and telphone='"+telphone+"'");
    	}
    	List<HashMap<String,Object>> list=Sqlhelper.select1(sql.toString());
    	Vector<Vector<String>>  users=new Vector<Vector<String>>();
    	if(list.size()>0){
    		for(HashMap<String,Object> map:list){
    			Vector<String> vector=new Vector<String>();
    			vector.add(map.get("id")==null?null:map.get("id").toString());
    			vector.add(map.get("name")==null?null:(map.get("name").toString()));
    			vector.add(map.get("username")==null?null:(map.get("username").toString()));
    			
    			vector.add(map.get("email")==null?null:(map.get("email").toString()));
    			vector.add(map.get("telphone")==null?null:(map.get("telphone").toString()));
    			vector.add(map.get("sex")==null?null:(map.get("sex").toString()));
    			vector.add(map.get("city")==null?null:(map.get("city").toString()));
    			users.add(vector);
    			
    		}	
    		
    	}
     	return users; 
    }
}
