package edu.taru.hr.view;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JSeparator;

/**
 * 主界面
 * @author 
 *
 */
public class Main extends JFrame{
	//声明一个导航栏
	JMenuBar bar=new JMenuBar();
	
public Main(){
	getContentPane().setBackground(Color.DARK_GRAY);
	
	setJMenuBar(bar);
	//声明一个一级权限管理菜单
	JMenu jmenu=new JMenu("权限管理");
	bar.add(jmenu);
	//声明多个二级菜单，并添加到一个一级菜单
	JMenuItem jmenuitem1=new JMenuItem("修改密码");
	jmenu.add(jmenuitem1);
	JMenuItem jmenuitem2=new JMenuItem("个人信息");
	jmenu.add(jmenuitem2);
	JMenuItem jmenuitem3=new JMenuItem("切换用户");
	jmenu.add(jmenuitem3);
	//二级菜单中的分割线
	JSeparator jseparator=new JSeparator();
	jmenu.add(jseparator);
	JMenuItem jmenuitem4=new JMenuItem("退出系统");
	jmenu.add(jmenuitem4);
	//声明一个一级用户管理菜单
	JMenu jmenu1=new JMenu("用户管理");
	bar.add(jmenu1);
	JMenuItem jmenuitem11=new JMenuItem("查询用户");
	jmenu1.add(jmenuitem11);
	JMenuItem jmenuitem22=new JMenuItem("添加用户");
	jmenu1.add(jmenuitem22);
	JMenuItem jmenuitem33=new JMenuItem("修改用户");
	jmenu1.add(jmenuitem33);
	JMenuItem jmenuItem44 = new JMenuItem("注册用户");
	jmenu1.add(jmenuItem44);
	//声明一个一级雇员管理菜单
	JMenu jmenu2=new JMenu("雇员管理");
	bar.add(jmenu2);
	//查询用户的二级菜单监听事件
	jmenuitem11.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent arg0) {
		Check register=new Check();//跳转注册界面
			
			
		}
		
	});
	
	
	//注册用户的二级菜单监听事件
	jmenuItem44.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent arg0) {
		Register register=new Register();//跳转注册界面
			
			
		}
		
	});
	
	//切换用户的二级菜单监听事件
	jmenuitem3.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent arg0) {
			Login login=new Login();//跳转登录界面
			Main.this.dispose();//隐藏登录界面
			
		}
		
	});
	//退出系统的二级菜单监听事件
	jmenuitem4.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent arg0) {
			System.exit(0);
			
		}
		
	});
	
	//面板的设计
	this.setTitle("\u4EBA\u529B\u8D44\u6E90\u7BA1\u7406\u7CFB\u7EDF---\u4E3B\u754C\u9762");
	this.setSize(770, 700);
	this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	this.setVisible(true);
	this.setLocationRelativeTo(null);
	getContentPane().setLayout(null);
	
	
}
}
