package edu.taru.hr.view;

import java.awt.Component;
import java.awt.TextField;

import javax.swing.ButtonGroup;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.JPasswordField;
import java.awt.Color;

import javax.swing.ImageIcon;

import edu.taru.hr.entity.User;
import edu.taru.hr.service.UserServiceImpl;

/**
 * 修改界面
 * @author 
 *
 */
public class Alter extends JFrame{
	
	UserServiceImpl userServiceImpl=null;
	private JTextField textField_3;
	private JTextField textField_4;
	private JTextField textField_1;
	private JTextField textField_2;
	
	public Alter(final User user) {

		
		
		getContentPane().setBackground(Color.DARK_GRAY);
	
		getContentPane().setLayout(null);
		
		JLabel label = new JLabel("\u59D3\u540D");
		label.setBounds(10, 34, 54, 15);
		getContentPane().add(label);
		
		JLabel label_1 = new JLabel("\u7528\u6237\u540D");
		label_1.setBounds(10, 87, 54, 15);
		getContentPane().add(label_1);
		
		JLabel lblEmail = new JLabel("\u8054\u7CFB\u7535\u8BDD");
		lblEmail.setBounds(10, 183, 54, 15);
		getContentPane().add(lblEmail);
		
		JLabel lblEmail_1 = new JLabel("email");
		lblEmail_1.setBounds(10, 135, 68, 15);
		getContentPane().add(lblEmail_1);
		//逻辑组件
		ButtonGroup bg=new ButtonGroup();
		//籍贯
		final String [] citys={"北京","上海","广州","深圳","浙江","杭州","重庆市","河北","山东","湖北","湖南","河南","云南","广西","新疆"};
		//emile
		textField_3 = new JTextField();
		textField_3.setText(user.getEmail());
		textField_3.setBounds(79, 132, 147, 21);
		getContentPane().add(textField_3);
		textField_3.setColumns(10);
		//telphone
		textField_4 = new JTextField();
		textField_4.setText(user.getTelphone());
		textField_4.setBounds(79, 180, 147, 21);
		getContentPane().add(textField_4);
		textField_4.setColumns(10);
		
		
		//面板的设计
		this.setTitle("人力资源管理系统---修改界面");
		this.setSize(555, 501);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setVisible(true);
		this.setLocationRelativeTo(null);
		getContentPane().setLayout(null);
	//修改
		JButton button = new JButton("\u786E\u8BA4");
		button.setBounds(336, 235, 93, 44);
		getContentPane().add(button);
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				UserServiceImpl userServiceImpl=new UserServiceImpl();
				User users =user;
				
				users.setName(textField_1.getText());
				users.setUsername(textField_2.getText());
				users.setEmail(textField_3.getText());
				users.setTelphone(textField_4.getText());
					try {
						//调用注册方法
						userServiceImpl.alterUpdate(user);
						JOptionPane.showMessageDialog(Alter.this, "修改成功！");
					} catch (Exception e2) {
						JOptionPane.showMessageDialog(Alter.this, "修改失败！");
					}	
					
			}
			
			
		});
		
		//退出
		JButton btnNewButton = new JButton("\u9000\u51FA");
		btnNewButton.setBounds(336, 301, 93, 44);
		getContentPane().add(btnNewButton);
		
		JLabel lblNewLabel_1 = new JLabel("");
		lblNewLabel_1.setIcon(new ImageIcon(Alter.class.getResource("/image/wsp.jpg")));
		lblNewLabel_1.setBounds(253, 10, 200, 202);
		getContentPane().add(lblNewLabel_1);
		//name
		textField_1 = new JTextField();
		textField_1.setText(user.getName());
		textField_1.setBounds(74, 31, 145, 21);
		getContentPane().add(textField_1);
		textField_1.setColumns(10);
		//username
		textField_2 = new JTextField();
		textField_2.setText(user.getUsername());
		textField_2.setBounds(74, 84, 145, 21);
		getContentPane().add(textField_2);
		textField_2.setColumns(10);
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Alter.this.dispose();//隐藏注册界面
				Check register=new Check();//跳转注册界面
			}
		});
	
	}
}
