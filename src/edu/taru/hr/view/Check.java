 package edu.taru.hr.view;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.BoxLayout;
import javax.swing.JScrollPane;
import javax.swing.JTable;








import edu.taru.hr.entity.User;
import edu.taru.hr.service.UserServiceImpl;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.Vector;

public class Check extends JFrame{
	private JTextField textField;
	static User user =new User();
	String [] s={"id","真实姓名","用户名","邮箱","电话","性别","籍贯"};
	String[][] s1=new String[13][7] ;
	private JTable table = new JTable(s1,s);
	 UserServiceImpl userServiceImpl=new UserServiceImpl();
	 private JTextField textField_1;
	 private JTextField textField_2;
	 private JTextField textField_3;
	
	public Check() {
		this.setTitle("蓝桥人力管理");
		this.setSize(738,384);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setVisible(true);
		this.setLocationRelativeTo(null);
		getContentPane().setLayout(null);
		
		
		
		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(null, "\u641C\u7D22\u9762\u677F", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel.setBounds(0, 0, 732, 87);
		getContentPane().add(panel);
		panel.setLayout(null);
		
		JLabel lblid = new JLabel("\u8F93\u5165\u7528\u6237\u540D\u6216\u8005id");
		lblid.setBounds(244, 25, 103, 15);
		panel.add(lblid);
		
		textField = new JTextField();
		textField.setBounds(368, 22, 110, 21);
		panel.add(textField);
		textField.setColumns(10);
		
		JButton button = new JButton("\u641C\u7D22");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//多条件查询
				String name =textField_1.getText();
				String email=textField_2.getText();
				String telphone=textField_3.getText();
				Vector<Vector<String>> vectors=userServiceImpl.selectByOthers(name, email, telphone);
				
				
				/*//搜索
				String user=textField.getText() ;
				Vector<Vector<String>> vectors=userServiceImpl.sousuo(user);*/
				if(vectors.size()==0){
					JOptionPane.showMessageDialog(Check.this, "没有可查询的人");
				}
				else{
				Vector<String> header=new Vector<String>();
				for(int i=0;i<s.length;i++){
					header.add(s[i]);
				}
				DefaultTableModel tableModel=new DefaultTableModel(vectors,header);
				table.setModel(tableModel);
				table.updateUI();
			}}
		});
		button.setBounds(508, 21, 69, 23);
		panel.add(button);
		
		JButton button_1 = new JButton("\u5237\u65B0");
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//刷新
				Vector<Vector<String>> vectors=userServiceImpl.reach();
				Vector<String> header=new Vector<String>();
				for(int i=0;i<s.length;i++){
					header.add(s[i]);
				}
				//表头的特性
				DefaultTableModel tableModel=new DefaultTableModel(vectors,header);
				table.setModel(tableModel);
				table.updateUI();
			}
		});
		button_1.setBounds(508, 54, 69, 23);
		panel.add(button_1);
		
		JButton button_2 = new JButton("\u5220\u9664");
		button_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//删除
			   int row=table.getSelectedRow();
			   try{
				   userServiceImpl.delect(table.getValueAt(row, 0).toString());
				   JOptionPane.showMessageDialog(Check.this, "删除成功");
			   }catch(Exception es){
				   JOptionPane.showMessageDialog(Check.this, "删除失败");
			   }
					//刷新
					Vector<Vector<String>> vectors=userServiceImpl.reach();
					Vector<String> header=new Vector<String>();
					for(int i=0;i<s.length;i++){
						header.add(s[i]);
					}
					DefaultTableModel tableModel=new DefaultTableModel(vectors,header);
					table.setModel(tableModel);
					table.updateUI();
			}
		});
		button_2.setBounds(608, 21, 69, 23);
		panel.add(button_2);
		
		JButton btnNewButton = new JButton("\u4FEE\u6539");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//修改
				try{
				int row=table.getSelectedRow();
				String id=table.getValueAt(row, 0).toString();
				UserServiceImpl userServiceImpl=new UserServiceImpl();
				User user=userServiceImpl.alterReach(id);
				Alter alter =new Alter(user);
				}catch(Exception es){
					JOptionPane.showMessageDialog(Check.this, "请选择你你要修改的用户");
				}
				
			}
		});
		btnNewButton.setBounds(608, 54, 69, 23);
		panel.add(btnNewButton);
		
		JLabel label = new JLabel("\u8F93\u5165\u59D3\u540D");
		label.setBounds(33, 25, 54, 15);
		panel.add(label);
		
		textField_1 = new JTextField();
		textField_1.setBounds(97, 22, 103, 21);
		panel.add(textField_1);
		textField_1.setColumns(10);
		
		JLabel label_1 = new JLabel("\u8F93\u5165\u90AE\u7BB1");
		label_1.setBounds(33, 54, 54, 15);
		panel.add(label_1);
		
		textField_2 = new JTextField();
		textField_2.setBounds(97, 55, 103, 21);
		panel.add(textField_2);
		textField_2.setColumns(10);
		
		JLabel label_2 = new JLabel("\u8F93\u5165\u7535\u8BDD");
		label_2.setBounds(256, 58, 54, 15);
		panel.add(label_2);
		
		textField_3 = new JTextField();
		textField_3.setBounds(368, 55, 114, 21);
		panel.add(textField_3);
		textField_3.setColumns(10);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new TitledBorder(null, "\u6570\u636E\u5217\u8868", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_1.setBounds(0, 90, 732, 264);
		getContentPane().add(panel_1);
		panel_1.setLayout(new BoxLayout(panel_1, BoxLayout.X_AXIS));
		
		JScrollPane scrollPane = new JScrollPane();
		panel_1.add(scrollPane);
		
		
		scrollPane.setViewportView(table);
	}
}
