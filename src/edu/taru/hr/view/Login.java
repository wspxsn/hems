package edu.taru.hr.view;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import edu.taru.hr.entity.User;
import edu.taru.hr.service.LoginServiceImpl;



/**
 * 登录界面
 * @author 
 *
 */
public class Login extends JFrame {
	JButton login_button=new JButton("登录");//登录按钮
	JButton exit_button=new JButton("退出");//退出按钮
	JTextField login_text=new JTextField();//登录文本框
	JPasswordField pass_text=new JPasswordField();//密码文本框
	JLabel login_lable=new JLabel("用户名:");//登录lable
	JLabel lexit_lable=new JLabel("密码:");//退出lable
	private final JLabel lblNewLabel = new JLabel("");//存放图片的lable
	
	
	
	
	public Login(){
		getContentPane().setBackground(new Color(255, 160, 122));
		getContentPane().setLayout(null);//绝对定位布局
		//登录按钮的定位
		login_button.setBounds(262, 255, 71, 37);
		getContentPane().add(login_button);
		//退出按钮的定位
		exit_button.setBounds(262, 305, 71, 37);
		getContentPane().add(exit_button);
		//登录lable的定位
		login_lable.setBounds(10, 255, 42, 25);
		getContentPane().add(login_lable);
		//退出lable的定位
		lexit_lable.setBounds(10, 311, 42, 25);
		getContentPane().add(lexit_lable);
		//登录文本框的定位
		login_text.setBounds(72, 256, 170, 36);
		getContentPane().add(login_text);
		//密码文本框的定位
		pass_text.setBounds(72,311, 170, 31);
		getContentPane().add(pass_text);
		//图片添加
		lblNewLabel.setIcon(new ImageIcon(Login.class.getResource("/image/wsp.jpg")));
		lblNewLabel.setBounds(0, 0, 360, 245);
		getContentPane().add(lblNewLabel);
		
		/**
		 * 登录按钮的触发事件
		 */
		login_button.addActionListener(new ActionListener(){

			public void actionPerformed(ActionEvent arg0) {
				//获得用户名
				String username=login_text.getText();
				//获得用密码
				String password=new String(pass_text.getPassword());
				//获得用户
				LoginServiceImpl loginServiceImpl =new LoginServiceImpl();
				try {
					User user =loginServiceImpl.login(username, password);
					if(user==null){
						JOptionPane.showMessageDialog(Login.this, "用户或者密码错误");
					}else{
						Main main=new Main();//跳转主界面
						Login.this.dispose();//隐藏登录界面
						
					}
				} catch (Exception e) {
					JOptionPane.showMessageDialog(Login.this,"登录失败");
				}		
			}
			
		});
		/**
		 * 退出按钮的触发事件
		 */
		exit_button.addActionListener(new ActionListener(){

			public void actionPerformed(ActionEvent arg0) {
			System.exit(0);//直接退出系统
			}
			
		});
		
		//面板的设计
		this.setTitle("人力资源管理系统");
		this.setSize(371, 405);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setVisible(true);
		this.setLocationRelativeTo(null);
	}
}
