package edu.taru.hr.view;

import java.awt.Component;
import java.awt.TextField;

import javax.swing.ButtonGroup;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.JPasswordField;






import java.awt.Color;

import javax.swing.ImageIcon;

import edu.taru.hr.entity.User;
import edu.taru.hr.service.UserServiceImpl;

/**
 * 注册界面
 * @author 
 *
 */
public class Register extends JFrame{
	UserServiceImpl userServiceImpl=null;
	private JTextField textField;
	private JTextField textField_3;
	private JTextField textField_4;
	private JPasswordField passwordField;
	private JPasswordField passwordField_1;
	private JTextField textField_1 = new JTextField();
	public Register() {
		getContentPane().setBackground(Color.DARK_GRAY);
	
		getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("姓名:");
		lblNewLabel.setBounds(10, 31, 54, 15);
		getContentPane().add(lblNewLabel);
		
		JLabel label = new JLabel("密码:");
		label.setBounds(10, 82, 54, 15);
		getContentPane().add(label);
		
		JLabel label_1 = new JLabel("确认密码:");
		label_1.setBounds(10, 128, 54, 15);
		getContentPane().add(label_1);
		
		JLabel lblEmail = new JLabel("email:");
		lblEmail.setBounds(10, 264, 54, 15);
		getContentPane().add(lblEmail);
		
		JLabel label_2 = new JLabel("联系电话");
		label_2.setBounds(10, 222, 68, 15);
		getContentPane().add(label_2);
		
		JLabel label_3 = new JLabel("性别");
		label_3.setBounds(10, 176, 54, 15);
		getContentPane().add(label_3);
		
		JLabel label_5 = new JLabel("籍贯:");
		label_5.setBounds(10, 316, 54, 15);
		getContentPane().add(label_5);
		
		//女
		final JRadioButton radioButton = new JRadioButton("女");
		radioButton.setBounds(86, 172, 75, 23);
		getContentPane().add(radioButton);
		//男
		final JRadioButton radioButton_1 = new JRadioButton("男");
		radioButton_1.setBounds(163, 172, 68, 23);
		getContentPane().add(radioButton_1);
		//逻辑组件
		ButtonGroup bg=new ButtonGroup();
		bg.add(radioButton);
		bg.add(radioButton_1);
		//籍贯
		final String [] citys={"北京","上海","广州","深圳","浙江","杭州","重庆市","河北","山东","湖北","湖南","河南","云南","广西","新疆"};
		final JComboBox comboBox = new JComboBox(citys);
		comboBox.setBounds(84, 313, 147, 21);
		getContentPane().add(comboBox);
		//姓名
		textField = new JTextField();
		textField.setBounds(86, 28, 147, 21);
		getContentPane().add(textField);
		textField.setColumns(10);
		//联系电话
		textField_3 = new JTextField();
		textField_3.setBounds(86, 219, 147, 21);
		getContentPane().add(textField_3);
		textField_3.setColumns(10);
		//email
		textField_4 = new JTextField();
		textField_4.setBounds(86, 261, 147, 21);
		getContentPane().add(textField_4);
		textField_4.setColumns(10);
		//密码
		passwordField = new JPasswordField();
		passwordField.setBounds(88, 79, 145, 21);
		getContentPane().add(passwordField);
		//确认密码
		passwordField_1 = new JPasswordField();
		passwordField_1.setBounds(88, 125, 145, 21);
		getContentPane().add(passwordField_1);
		
		
		//面板的设计
		this.setTitle("人力资源管理系统---注册界面");
		this.setSize(555, 501);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setVisible(true);
		this.setLocationRelativeTo(null);
		getContentPane().setLayout(null);
		//注册事件
		JButton button = new JButton("注册");
		button.setBounds(346, 273, 93, 44);
		getContentPane().add(button);
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				UserServiceImpl userServiceImpl=new UserServiceImpl();

				User user=new User();
				//获得用户名
				String name=textField.getText();
				//获得用密码
				String password=new String(passwordField.getPassword());
				String password1=new String(passwordField_1.getPassword());
				//性别判断
				String sex="女";
				if(radioButton_1.isSelected()){
					sex="男";
				}
				//判断密码是否相等
				//获取测试
				int index=comboBox.getSelectedIndex();
				String city=citys[index];
				//获取备注
				
				String username=textField_1.getText();
				
				user.setUsername(username);
				user.setName(name);
				user.setPassword(password1);
				user.setCity(city);
				user.setEmail(textField_4.getText());
				user.setTelphone(textField_3.getText());
				user.setSex(sex);
				if(!password.equals(password1)||password1.isEmpty()||name.isEmpty()){
					JOptionPane.showMessageDialog(Register.this, "注册失败！");
				}
				else{
					try {
						//调用注册方法
						userServiceImpl.register(user);
						JOptionPane.showMessageDialog(Register.this, "注册成功！");
					} catch (Exception e2) {
						JOptionPane.showMessageDialog(Register.this, "注册失败！");
					}
					
				}
			
			}
		});
		
		//重置事件
		JButton btnNewButton = new JButton("重置");
		btnNewButton.setBounds(346, 352, 93, 44);
		getContentPane().add(btnNewButton);
		
		JLabel lblNewLabel_1 = new JLabel("");
		lblNewLabel_1.setIcon(new ImageIcon(Register.class.getResource("/image/wsp.jpg")));
		lblNewLabel_1.setBounds(253, 10, 276, 230);
		getContentPane().add(lblNewLabel_1);
		
		JLabel label_6 = new JLabel("\u7528\u6237\u59D3\u540D");
		label_6.setBounds(10, 367, 54, 15);
		getContentPane().add(label_6);
		
		
		textField_1.setBounds(83, 363, 148, 24);
		getContentPane().add(textField_1);
		textField_1.setColumns(10);
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Register.this.dispose();//隐藏注册界面
				Register register=new Register();//跳转注册界面
			}
		});
	
	}
}
